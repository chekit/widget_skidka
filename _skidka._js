var com = com || {};
com.rooxteam = com.rooxteam || {};
com.rooxteam.widgets = com.rooxteam.widgets || {};

(function() {
	'use strict';

	// var _base = 'http://coupons-web.zavyalova.fenrir.immo/';
	var _base = 'http://stage.skidka.mts.ru/';

	var getData = function (base, uri, config) {
		var config = config || {};

		return $.ajax({
			url: _base + uri,
			type: 'POST',
			data: config,
			crossDomain: true
		});
	};

	var templates = {
		main: [
			'<div class="skidka-widget__cover">',
				'<span class="skidka-widget__cover-discount">{{discount_percent}}%</span>',
				'<img src="{{img}}" class="skidka-widget__cover-image">',
			'</div>',
			'<div class="skidka-widget__content">',
				'<p class="skidka-widget__content-description">',
					'<a href="http://skidka.mts.ru/deal/{{deal_id}}">{{title}}</a>',
				'</p>',
				'<div class="skidka-widget__content-order">',
					'<button class="btn--red">Заказать</button>',
					'<p class="cost-val">{{price}}</p>',
				'</div>',
			'</div>'
		].join('\n'),
		loading: [
			'<div class="skidka-widget__loading">',
				'<img src="/img/box-loader.gif" class="skidka-widget__loading-img">',
			'</div>'
		].join('\n'),
		popup: [
			'<div class="popup">',
				'<div class="modal">',
					'<button type="button" class="modal__close">Закрыть</button>',
					'<h2 class="modal__title">Благодарим вас за заказ!</h2>',
					'<p class="modal__text">Информация отправлена на ваш телефон по SMS. Желаем приятных эмоций!</p>',
					'<div class="modal__btn">Выбрать ещё купоны</div>',
					'<h2 class="modal__title">Сервис недоступен</h2>',
					'<p class="modal__text">Попробуйте повторить запрос позднее</p>',
				'</div>',
			'</div>'
		].join('\n')
	};

	function Skidka (moduleId, moduleBaseUrl, widgetBodyId, widgetRefreshId) {
		this.widgetBaseUrl = moduleBaseUrl;

		var viewPrefix = "",
				commonUrls;

		// Пути
		if (typeof moduleBaseUrl == 'undefined' || moduleBaseUrl === "./") {
				commonUrls = widgetUrls;
				this.widgetBaseUrl = commonUrls[moduleId].substring(0, commonUrls[moduleId].lastIndexOf('/'));
		} else {
				this.widgetBaseUrl = moduleBaseUrl;
		} // end if

		this.widgetImagesUrl = this.widgetBaseUrl + viewPrefix + "/img";

		this.container = document.getElementById(widgetBodyId);
		this.refresh = document.getElementById(widgetRefreshId);
		this.prev = 0;
		this.ln = null;
		this.data = null;

		this.render();
	}

	Skidka.prototype = {
		showLoading: function () {
			this.container.innerHTML = this.templates['loading'].replace(/{{widgetImagesUrl}}/gi, this.widgetImagesUrl);
		},

		generateRandom: function () {
			var _self = this;

			var random = Math.floor( Math.random() * _self.ln );

			if (random === _self.prev) {
				random = _self.generateRandom();
			}

			this.prev = random;

			return random;
		},

		setListeners: function () {
			var _self = this;

			_self.refresh.addEventListener('click', function (e) {
				e.preventDefault();

				_self.showLoading();
				setTimeout(_self.parse.bind(_self), 200);
			}, false);
		},

		parse: function (deals) {
			var rn, frag, deal;

			if (!this.ln) this.ln = this.data.length;

			rn = this.generateRandom();
			deal = this.data[rn];

			frag = this.templates['main']
							.replace(/{{discount_percent}}/ig, deal.discount_percent)
							.replace(/{{img}}/ig, deal.image)
							.replace(/{{deal_id}}/ig, deal.deal_id)
							.replace(/{{title}}/ig, deal.title)
							.replace(/{{price}}/ig, deal.price > 0 ? 'Стоимость купона ' + deal.price + ' руб.' : 'Бесплатно');

			this.container.innerHTML = frag;
		},

		render: function () {
			var _self = this;

			_self.showLoading();

			getData('?r=default/defaultCity')
				.then(function (response) {
					console.log(response);

					getData(
						'?r=default/filterDeals',
						{
							'filter': {
								cities: response.payload.data.defaultCity.city_id
							}
						}
					)
						.then(function (response) {
							console.log('--> SUCCESS', response);
							_self.data = response.payload.deals;
			
							_self.setListeners();
							_self.parse();
						})
						.fail(function (err) {
							console.warn('--> FAIL', err);
						});
				})
				.fail(function (err) {
					console.warn('--> FAIL', err);
				});
		}
	};

	com.rooxteam.widgets.Skidka = Skidka;
})();