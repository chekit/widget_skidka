var com = com || {};
com.rooxteam = com.rooxteam || {};
com.rooxteam.widgets = com.rooxteam.widgets || {};

(function () {
  'use strict';

  // var _base = 'http://coupons-web.zavyalova.fenrir.immo/';
  var _base = 'http://stage.skidka.mts.ru/';
  var _msisdn = com.rooxteam.auth ? com.rooxteam.auth.getPrincipal() : null; 

  function Skidka(moduleId, moduleBaseUrl, widgetBodyId, widgetRefreshId, widgetOrderId) {
    var viewPrefix = "",
      commonUrls;

    this.widgetBaseUrl = moduleBaseUrl;

    // Пути
    if (typeof moduleBaseUrl == 'undefined' || moduleBaseUrl === "./") {
      commonUrls = widgetUrls;
      this.widgetBaseUrl = commonUrls[moduleId].substring(0, commonUrls[moduleId].lastIndexOf('/'));
    } else {
      this.widgetBaseUrl = moduleBaseUrl;
    } // end if

    this.widgetImagesUrl = this.widgetBaseUrl + viewPrefix + "/img";

    this.container = document.getElementById(widgetBodyId);
    this.refresh = document.getElementById(widgetRefreshId);
    // this.order = document.getElementById(widgetOrderId);

    this.prev = 0;
    this.ln = null;
    this.deals = null;

    this.init();
  }

  Skidka.prototype = {
    templates: {
      main: [
        '<div class="skidka-widget__cover">',
        '<span class="skidka-widget__cover-discount">#discount_percent#%</span>',
        '<img src="#img#" class="skidka-widget__cover-image">',
        '</div>',
        '<div class="skidka-widget__content">',
        '<p class="skidka-widget__content-description">',
        '<a href="http://skidka.mts.ru/deal/#deal_id#">#title#</a>',
        '</p>',
        '<div class="skidka-widget__content-order">',
        '<a href="#order_link#" class="btn--red" id="js-order">Заказать</a>',
        '<p class="cost-val">#price#</p>',
        '</div>',
        '</div>'
      ].join('\n'),
      loading: [
        '<div class="skidka-widget__loading">',
        // '<img src="/img/box-loader.gif" class="skidka-widget__loading-img">',
        '<img src="data:image/gif;base64,R0lGODlhPAA8AOZ8AP////P09P39/f7+/vT19f8gKf/Fx//S0/f4+P/5+f/Jy//P0f++wv/Y2f8cJv+Wmv+3u/8aIv8qMv+nq/9yd/76+v8XIP96f/+1uP+Bhf/V1v99gv8kLv/c3f+wsv+hpv9TWv+fo/+Znv8tNf/Mzf/4+P8zO/9PVv9aYP9ASP/Nzv+Eif9MVP/y8v+Rlf+Hi//Bw/+rrv9DSv83P//r6/+Ok/+6vf/m5/9rcf/Z2vX29vj4+Pn6+v+cofz8/Pz9/fr6+v9hZv9obv/Dxf/b3P/h4v+zt/+xtf9cYv729v8oMP+tsP/v8P/e3/+Kjv+9wP/W2P/k5f8xOf80PP88RP/a2//u7/9jav+Sl/+Lj/8+Rv9HTv85Qf93fP9eZP9scv9JT/9wdf9ETP91ev/U1f9mbP9VXP38/P+7vvb39//Hyf+kqP/w8P9ka//09P+lqf+jp/+qrf+YnP+Jjf/p6v90ef/s7f/o6f/z9P9XXv9LUf9RWP///wAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/wtYTVAgRGF0YVhNUDw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNSAoV2luZG93cykiIHhtcDpDcmVhdGVEYXRlPSIyMDE2LTAyLTE5VDEyOjAxOjI3KzAzOjAwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAxNi0wMi0xOVQxMjozNDoxNSswMzowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAxNi0wMi0xOVQxMjozNDoxNSswMzowMCIgZGM6Zm9ybWF0PSJpbWFnZS9naWYiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RjBFNkZCRjdENkVCMTFFNUI4NTBDMkMwODY4RjcwMUUiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RjBFNkZCRjhENkVCMTFFNUI4NTBDMkMwODY4RjcwMUUiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpGMEU2RkJGNUQ2RUIxMUU1Qjg1MEMyQzA4NjhGNzAxRSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpGMEU2RkJGNkQ2RUIxMUU1Qjg1MEMyQzA4NjhGNzAxRSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgH//v38+/r5+Pf29fTz8vHw7+7t7Ovq6ejn5uXk4+Lh4N/e3dzb2tnY19bV1NPS0dDPzs3My8rJyMfGxcTDwsHAv769vLu6ubi3trW0s7KxsK+urayrqqmop6alpKOioaCfnp2cm5qZmJeWlZSTkpGQj46NjIuKiYiHhoWEg4KBgH9+fXx7enl4d3Z1dHNycXBvbm1sa2ppaGdmZWRjYmFgX15dXFtaWVhXVlVUU1JRUE9OTUxLSklIR0ZFRENCQUA/Pj08Ozo5ODc2NTQzMjEwLy4tLCsqKSgnJiUkIyIhIB8eHRwbGhkYFxYVFBMSERAPDg0MCwoJCAcGBQQDAgEAACH5BAUAAHwALAAAAAA8ADwAAAf/gACCg4SFhoeIiYqLjI2Oj5CRkpOTA5aXlJmGlgKdnp+elpqRA583SxtIWhIOTU1FUTRsCZ+Yo4meUVgyDhG+FsAdHUREVTkNTTS0n7ebnVBfBQ7TvhHAFsLExg0NUBpRSczNgqU0XQXo072/wcPFx90aGmRRywLjpRAm6OnU1e3v4Hmbd0ADk04DbgkokYEDP34OCpxwgqGBlRJnKpRoQaNIPIIHQkY5IyAhpVJ42nBY+ZCLnBugYgpIQKMKyAMLFnSoULKSgCQolChZ6dDEmxIykwo4YwdKyJwLVDTgKalUiSsShBKlYEWp15lFcOZUoSLHGZOPOq2QwFaohAlf/+MKoDFWBQkSTe6lFQCBrV8pDGT+AIJAB4EAAQjoQADkh0wmUe8qUEAD7aJONKiMGMF2RGBQPnYcRky6NIEdPmIysTtZAYkSlhGVEnBh82YJMWLyGF26N2kCPGLSaK3GQBW9ip6ZkCJlc5eYCHxL740gZgcFxQ0YaBG70Ow6y5mn6PopzfTzpNOASkBCu3YNyA+VujHFhH0pS0BFR8+/uin3QxiQRHeDdBLCFPWZwII9AvDA34MBBOfJGSQMYSEMeREIQCcoIIjgB5/4wBuE0xGQmidFDAHDigp0gogAd8wgo4x0fLIDiQ/u8EkSKzLgoxs9GSIABjPO0MYnP4yIo/90BDjmiQo++ggTgQLUwMWVLn0CxJIPAvFJBww8IWYDQRYiAAVUpMnFZ53sx+V5/nVCh5hooKFCmYQIgIIWaVKRlyc6vImeDp+0YMOhNgwR3yClgKHFo1oc5ImSgpr2SQk2QKBpYPIJkMKnnzJYKXqfVKDpqYuS4ymoKYg66nSlYiArBhCkCkApLMigqwySdkLpqwRcOisGNthaihe7ivFnJ4G+2huhnrBhxLRGcOrMBWJkK4ainrjpLGJxChDFEeQeYYCxAjywxbpbhKDlt6V56QkZHtR7xAG2bogGGPyCQQGSvwra5CcM1FtvXi/SAIYeerDAAg02whuAjp64scT/xRdz96IAQjjscH6eiPitiZ9AEcPJMdSqYSlxOHzCCVcw6KCzEnZSwRETTBBHDPgmd8fLL+9R6yfevhmuAFXknHMcLeSr6gsn7AECCEE0XV6l6u0Ywxpv5KyohgV2MPXYLkBndExDfAAH12/Y4TSjArgwtRlm5PGEbgH7BlxMDYTwwd9wnAt2nlYEQXceKHhxJ2iioXfaiZ8UEcLkf0+Ax+B5CmAA4iiggMQVCwhGmGGIKcaYk6BI3kMPk4eA8F49dI4EEl5cwaZcSZEhwu6rhyA4JKUk8MLsXngRRBAiWI37J25A8MADcvCOQQWYd8pQ8cdf0QYFMDAYVwUHiIDF7PPQi3AELSf9lEX2bZQhhBAX2NCrUi0oIEINLrgw/vMY0FI9LjP5wBWuUAb34QAHXwiDCzBwgBu4oQIVcMMdNMCADzjBCVmoAf6wMD4Y8OR/ipgNCcLgPiEcMIEUoMAYunCBC2xgAxnIwApe8AIMajB/IiACQpoxGzaEwIQIDEMKVchCF8JQhjScgw1rYAM84ImHnShCD76AQgrUYYUtfGEMVzBDJWbBCDV64jg21AkrQOAFKbxiEbUowxmKwACSAmEmZlNGEnigB1nIwAtfgAU4QOAAvZIjPi7BCZmIYoyITKQiF8nIRjoyEoEAACH5BAUAAHwALAUABQAyADIAAAf/gHyCg4SFhEcvbVtTEkomMkEZHjeGlZaXgzciJxKNShwcBaKiDqUyLlGYqpU5XVIjnZ6go6SlEREOQhqrq1YZJq+wnUqfobSlDrfKFDS8lk8pwMHCjaDGtcm3FtscLc6ECU5TJuRS0yguNjlMfCVMDRBOJ9kR2xYdHc3fbhQzU+PltoTgdePBFHv3OhBJ5UzIjIf/HsVI8K3dBwkJiRCpUmRVgjBcuDz0d6FiITt38G2skqMjphpUqIScwcWDyUpMOlRh2aABJUsMtAiNKcPATUstOuTo2QAKu0osUqQQmsLoUaRLm0JpQLHQCxlSpx65islKUw1oGQ6qIkYG2BQZ/8iqioJWAxkybghlaOt2z1O5lio0sHvgQJNBd8CIWSzDCGBVVsgULlxC0IQtmLcgebyqweQFP4WAGb0lDmdVdA4sWA2FDw0WevSAYaHvtKUSq3OXeMKiNwsKtlXlWK1CBY1NJ3p/CI4pSvHiRTKcmH7CKvNKVkhoJ5EDx57vJ1xeN+RGuwIFB5CAWA/C23hDCc6fV8F+/ftKZ+SfN8Of/31DZxggoAFqBJHHgXm4958gCQxogAIUoCAhCuItiIeDKswhIRJIKLDgIDQMIeIQB3zAIYcxfChIEzDAIGIHQ3ghoxdxqagCAy3CcIMdXgThYxBWfJgEA0QSmQQfF/x4Bf8GHzbxxBNEqiGIEVdU2UYXH8KAxpNPdCAIDWW0UcaYDPxXhA1obpmXIA+UIcSbY9y3GwQQoLkAIUW8KQQOOAw03gIY0EknG4WEwCcOX3xhnW1FGIHBoxDcWQgTdSD6RRh1HBDcDUZ0+qgNRxpCwqVhUEDBBa1xxqkHRxzhqFqGxFGqqXVcICVgHXiwhAesHqEpJg/QOkYXF0xAVhIGxBHDrrwuegkWdQxL7AUvKFCBSTksMcEEMSzrAQzXrlJCD11Mu8G5NRhA6CoHLAHHG9sqG8MQJnlwwb3nZpDBCi98AEMDNISaBA0dKHBECCF88MEa204QBwlHHbDCuRvoy+9lC05kUUMNLmDxwANyiNBDDwl/8O4ES1RoUgtL6LvvxRlv3PHHIY+MsMJrGBCqXDfEsMLPL8wRM8cegyyCyDfDEKRtTAwhQtBDz/zx0T0ssYCC1zFxABoxhPCAxz2sYcQQOWDtTCAAIfkEBQAAfAAsBQAFADIAMgAAB/+AfIKDhIWDdxhZXyApUyZULEIvR4aVlpeFISgzjiYmUiOhEqNKSiwPmKmWRBtUXDOcnaCipBy2HGFQqrsvKVSusFOytBKltwXIY7uXMCwpWlqur5yes6PFSrbI2yMYy4VYMjIpz79cXFcPDESEVTYuednbyA4OGyXffBdi4uTQIHDu7BJBZVu9ekhaLKOwRQy/cSc85BP0xkSBgxEinHCj6gKYLQ3FrWAycZCVLxgzosB36YEeMB+3gDFS0tCbAhkzWrhwaQiLny9PKKhZ6QkHnRYsUDJkB8WJnz/VELVkw0HSpBwq1TjB9SeEqZfWXLXQQSChDiBA7OGaBSwmHGT/O8hNQshJWrVe2Li9ZKWJXCJEbgyiYeYuiK97LzEBTKRKlQSCPOQxU1hI4lRNHOfIQUPQGBR5Jku8zHdzjgZNBKFYvZo0pgQNYsfmMwSJbRQbXGNqEhsKFCYfbNuOo/sSHd8aNNzI4qW5l6HFK7FJnrxIlyDYg0SJXilJcjJkquDIHoS7pQPoD0C5wp69+Urp0ZeZP/+9ofgHwgjZb9k+oQUALkDGBvwJsZ1/fCQR4AIaYIHDgzgsgCAfVqhgoQodxPDgF1/QhGAUF6pQhAIcfhFGDRNCQcKKJNBgRRgwUkABSfaVoMCNKwrygowyPuFfFDfeeIAgaPBYxwr+qWCA/xo3HmjHGHWMMUYXBrx3gwFYLkmXICFM2cUFK+DBXQJqZGlAA4REccGaaxIXHRRDDJGlQoRMwOYGG0Dn2g0w9BknmoWwsQKeeGYA6GU0MKBonwawVMgBhGaQwQvsJJboE4oyAAMdlxghqaQrvKDCXkWgYcMTmDKQAyYVfABqqC8YwVFNJSwAAQQ2oIGqhKqEsAKsczjxAK/5NGEDBrfiigYJ30zwwgvBZlFDDT2QQCcmbuQAwRFGYIDsraN+UwEE0UrrggtYPLCEAk3YwVIJVhRxwBNLeOABt90iS0ZNDbggbQ3npvuACCL0EEIIH8DxxgQTxBBDvfd2a4NgROEBAU7A6D6gMcEGI6www3E8bC+3Czg6FR0YYCGwHBwfnPAaIIvsgQI0XtaCAhM8wHIPHX/wAcwMx2ADFGJy10IDBmAQwxoJT3AEAyQ0UXQ+gQAAIfkEBQAAfAAsBQAFADIAMgAAB/+AfIKDhIWDNE8PF0EgW2InSHUuEHeGlpeYhxM4LGBgjjIyKSlaWlRcMyghN5mtlk1ZICcsLHqfYqGjpqczM1MzFzmurkwPILKztbe5pFS8vlMmJhl2w5dqQWbHeyfJt7i6p6i+0lJSVDbWhSEoedrH3ScUIQZNbIJsHTAibVxT0SbMjRixQh2fJE5QKDTzrs0SGq7ufGARUMrAERKE4BlWIgMSJArdBYFgkE+CGCkuSliJJIkrJ168fFTo4l7JQV0wrpSg5EorOEGCxETi5clNQzF0KlmaAZOCK1eCemmz4KglGCaWctiKwRKTMG2gBrmiwuolBhK2bjVRaZ2QMmX/wjIwi2mC2gIFKBQqgkPI2zIi6GaigLcwGUIicPQVQqGFYExMphQuIGSQnS+KFcN4nGnC5AJFBNkI8wXzhgqcMSWggteBgxqCXoSZ/SVdakxyXLtOwccKhd+/rdzGdKOAbgdFSACn4GJ4phMOIkif4GFMnToUujq/5ES69A09xogfc2D7JQzeI6Bw0qV9F1bmDTWQbsEClwwX8l/YGL+Qlfr1SaBffv0ZUgKAFjiwwYILolYgIR1E2EETKzC4AX8P8lGBhBO6kMGHGbSVYQlElEhEE2uAmIEGGQrSQhVVlBgFBCvUuMJcLdKQQw4w0nGAjS980CIfRTSwYw5MsPHC/5IvzGFTgQk0IKWUJfDRQ5NzOKFGhjRAAYWUHQhigBNkZiGCg/GdkYMGGngJER9MZCFnDTVU1Z8dZLDJZpWCeEAnnXK4FB8UB5CRZ2iD3EGnC4ySZF4RB0RaqKCDQMAoFlg8cJhzNCwg6QGIEtKCCJg+8IAITQzHxAKseqpBApbkYKqpcvQQqmCrqtDqAsJdAgOtIojQQwOP0UECCSrousCtlhwhR7A99BACDJSWlAARCihwbLJVnNFKAh4IK20IH8QhTEk3KGBAttqSQOwwCUAw7gcfwLGGBxq44UoSRahrgAFqsJsDmsMYEAK5H6zxxgQTxPAEGVEwMUgCLdzRAXkJMMAwxBD/AqxAE96W1MQE9S7McAxLeODBEUZggAEENtjwBAMMZMzxvwq8eVQSBijMcBwoq8yyyxDA/MTMNW9sABGwCmbFECcHvXLLL8OMBtIaQ+HYcBpAsETKRwxdtQ1XM6BAEdU6h0cRCxjwBAQvozGEuzekPUwgACH5BAUAAHwALAUABQAyADIAAAf/gHyCg4SFg3YGH04UbUgoQV8ZPQw0hpaXmIdGG0FeXo4oeWYgIHsnJyw4E3eZrZZRImVtV0GdoKKkpqh6YGArHa6uLR84QrK0to64pagsvFtiYk5WwZcLYzjFxsifyqMgpywsvdAyMicM1YUxYV/ZQsazKzEKRS0JCS1FiXXjW+UyUqSokUBdCREUwrR71wWCHVc04iARY05gCi0U3FTDQqFjuy9jYKgbZAREwItaqAgJJmJMnY4JQ7QYSYjJCpRUuHAJUxCThy5dXHY0QNPSkRRUcs6Y4QTTgQsXgI65oKHoJQMyuCydMcWGJTYroEalahXTECpLp0xJUWndhg1i/xWUzeSB6xQTJjYUupHh7dsJc1ttuIvXBBRCEzL03fAisCsxeKVIoTDIpmLFch1nWiJZyggpUQQZWEE6gwvNrsCMWD3igSARL0ivIIo6UwjWEljwYfOid282tTPdGSGhuIQoB5zMmfPiQ/BWKIxLWPLEiXUnIp9jclFciZJ5WcJnyaEdkw3v3q/0qMG+RtvyhqpwmM9BzIP2NZLAv0SfwwwXAAJYwX6WzFdAARJgoaCCBBpSwoEHKiHCAxQ+oF+Dg1gBYQEmwFHhAw9hKEgOG6ZgBIVyyAGMiHxAsKEXBoggowiZieiEAzg6kEEOM/ZwBIt8nJCjA3G00MORPcgk4v8NDkSQYxF8LJFkCCEcIOIDEWQZgRaCLEBlCB/EgGEJU2gZQRaCDPPBmh800OAHFligJZSCMLAmHGssUcJ+VkgQZ5xXEEIDnm9MMAFt5d3RwZ8RWEmIAYUaOsGKzzHRwaVxfmGIG0sYGkcMS9zwXAtEEHFpB02IakgRE3wK6hGqakZqFVWU2sFMl5AQA6geeHBEE5pZ0UADOdBKBB2twLBEr74aocKeZUUBxbDEVhGaKwz0esQRRmCABp3qnGFHAxpoMO2w4LpiwLbdYgABBDA0cWEmJdABxQFklGtuA9eqs0C77kJgAxoMkNAEDfeccQYfeFgRRQMLLHDAxPmaG+tsSDdAELANNjzxBAMMwDCEASQroAAJJKgQscQUN8DEXCUs8O7AH4csMskGmIyyyhFPfEC/gbGxABpo1CzyyCXrrALPCxQxL2pJNKEGyEfjbPLJS2twA7TwJXEDEQeQoIABapBwQANF0MC1OoEAACH5BAUAAHwALAUABQAyADIAAAf/gHyCg4SFg1YKSw8ZdV84YRc1awaGlZaXhFZoThRhYY44QmVtV0FBXkhdHjSYrZU3b11jFLSfOKGjpadISCgoLk2uwksXF7J1taCipKaovnlmZg9MwpZkKxvFsrOdyrm7vSjRICBIlNWDFRgZG9naY3UuRgtRhFEkMRm90OQgJycP0Alak4Fdu2IvGFgRdgSHGXJ7/rHYkERYgg8rChp8oUagIAhB/J1gQZKCqwoTXqzIWDAOHo+D2NQYSRIMmAutbMx5oXIlCZiGIIy0uWULlksNnDjZ+cJJFaCVFIAAU1SMGAaVWmDJonSOkxxQLanRs0WMDBknKmGoUSML1wVh/y9hMHs2xYpCdFywbWskLqYXdVOkADvIiIvDNR648XuJzR7BKbTgFMTmAZbDLg4wxmQkspbPNwSReGAZS4jNrUBQWU2lh6AJpEn/RH1pDRUuuFHwaSFHTuwWtC/RuT2jOJ8cIpLL8RAcU5vixY0YSJ5cQfNLD2ZM2Z4FQo/vPYJdr8RguwkTX2KECPF94XhDHc6fP/Fm/fr3lZjIl5Lig3//+BmSgBQESjHFfwAGSEgCIzQ4ggkTrCHhGgoSwoSDI1BxxBtvTDABNRXyQYQEJEqgBwMeelhEiHygUaIEZaiQ4gSahejCiy80EUccMcSAVYh5KCGkEkvg0WMMSyyxWP+FSnDAgZCC2JCkBx4QFqAITjq5hSANUEmlDRVqUUCWLgiCxxEeHKGmeO9NUMCbY9YjiAJqGmEEmPiZAGcBQlhopxEYYADXeBTsWQAZhagQKAYQQLBicxM44ACcJhWShA2MNooGHcGhUYCkko5wxyuNQmCDDQywstkTHEQAqgN9WaKBqTag8QQDcsa1hgMR9OrqZJeQYOutDDAARQJQVYCDBRb4GgEKJbRSAQnEMgADDGqE5hETTTDLbK8nLCntAsVeO4QBBpAQRbStJGBHE0R00IG3FiABHDo5mHsuumoooEEUVohbQRJs0NFEAzlUQUS881pwAbsC0WDAvgb0qwBgCSSosMDGBxxAhgYaNIBwwgxzECtUOaBbsQIXZ7zxAh1/rAEUIpNMxB0VMNZCAwawfLEKGsMcM8g0I1yFtrSVEMUBPwfdscdEN9AEDci+VwINRRChwdMaVFHEDUxULVAgACH5BAUAAHwALAUABQAyADIAAAf/gHyCg4SFg2wHEGsPcxkbKzU9RyRWhpaXmIcGPS8vKysZjhsXF11jYxRzNnaZrZY0RjVOTp2foRujpWN1FBRhYSFRrq54Ni41NVlOc56goqSmvL5fODgfbMOXRCIux8nLtbe5pr3T1UIUJNmFMA8PWN3Iyh8MUHdugng3Bxgu5jhChJRpA2cdnwQY3L2DV0OEAmytrKDZcK5MmStXaqw7IkKOQiwiFlQwyAfGGIFtrgQJsqKEKww9RHR0BwEfSUEtRKRc6cXLnFYGQvSIKZPMTUMM2gTpiQRJCExNQkgdGqLIUUsLynhpigKFAUt4JnyQKtXqVUsqgiDpiuJKpUJD/z7IHdvgLKYnXfOYMYOlkJ03a+DIhWE3Exa9ZkCA6EBoyIQ3gGMkKYypRRvFin/inMD5cQ7KmWxg3gPijiANcTofAd1KyJ4TsCcIghAjRmoNrDMtgX2ChRA+bpYsqR3DZu5XvVkov1HEg/AlDI5nqqNcjx4bBzxo92BU+qUPYMKDeWDgyBHtwrxbMrCl/ZYLT8ybh6i+UJMtYvIHgWCkvxGX9RXCRn4yyAACBggiGKAlBRa4RYIKLlhIChSmIIYNEGQIAYASClJhCicMoSEELXQoSBNapKgFCiTY4KINppnIABU0UkFBA2jk+ARjJorABRc0unDDE0Q+oY6JbczwI/8XRiTBwJNPTiYhHTNUWaUwCkAJg1kLfjDFFFWaIUgRMJQJgwISJqCHCV9OIYIgSQwBwxB03rDgEibkacIMhGhApwEGKJBAfUykIIUUedZBSAuANlpFfReMMMKhJkBRSA6NqqEAHd7FIIGkk15gSAkKGKCpApQcx4AUEnw6AhU0vKLArKiqwARrMLDa6qcQQDUrCSSosECshU0ggRJK7LpCJmc0AKwKwi7A5U1WUMDBtchKcMUwDUC7wLcHaGDHGQaVMIEJBRRwLQdKoCClK0REu8AB9DZAw6Ct3CAHF+mmu24beKxzRhHg0kuGBhpA0QQNLZQwEh9MQIGBEyc4YHFsv/5mcBQTZNB7wMEJN9BADlUQ0UEHFlgQwcoWO4CxFBiclUARHyMMhcg5kGwyyiqvHEHL6Y5BrF1JFGEzziXvnHLPP7v8BW65JUBDB0gToXTKPsuARXrqJcAEHVEU0UQHTShBBQobxMD1OoEAACH5BAUAAHwALAUABQAyADIAAAf/gHyCg4SFgy1VBhgTPXIPIh8eDGRshpaXmIcLHiE9PSKOD1guLjU1WU49BkyZrZZWQ3AfIZ2fIg+ipKaoL71LN66uSWoTa7KzniK3uaVZvC8rKxkeLcGXRR4TE2/HtLa4o81Oc73SGRkvB9aFKjFx2sXHRgpNdkkVFUk0VbFO0OYbAh5ZJ8jAkhjutL1ZcqBaKzYGXJzLEPDChR4lglWA4cHDwRgTPOQgyKeCAicVLXZ5EMzAkY5LDhpIQnIQnjgbVHYZI6LVASNHXnbsUNOQggtddtahEAPTDQxGgB4xAqyoISgbxiylQEGFpSQ2MIiNWtWqIQ1juFII04VVoQUQ/yCIxVDELCYDXMN8+RKiEJu4cTF4tYvpg94vOHDUHbTAho24TzISxnQhMQ4hcgYleYLGsY3Fky8NuSykNA1BTZ6oRjMkdKYKF0qXKTOQjwIGDFQ3cZ0Jwuw2bS7wSYK7OE3el6yUucL8Co0bMKIzGIz80oog2IPA6DAkOozd1S/FwO7FC5wDQ9IPOR3eEonyXpA4IWGgvgGH7QtFQcIfCQUF9hmQQH6GtIDCgSgIEaABZxBoCIIoIKHGhBM26CAhZmRoBgokKOChAgNeKEgLIJRoRhAHfKiAGyIKUkSJJeKQAwk0kmBFi3wYsAeMGRRBowoqRIHjBycUeUIPNAAJZP8DOI7BgpFolKDCAlQuIJmDNLCgpZbAQFHlAnSIGAMYeujBQhmC3EDlAQdAYSGBSGwBxpxwCFICm3ja4SAGYmzhpx5lNYEnGUwSCIIMYvSZASFuHEAGGRpoIGR7K8hgaaJE6BdppFDcWJ0RKYRq6QuGJNAApw00gF9oBsigRagpnHAJE1BAkWoDRKxqV6tUaPFqCgw4dWsOORDhll0eUMEFF71qUUMrRTRAbBVEEHFDBWZtMMUMMyxLBQUhZlJEDtQS0UEHEnxwZTAJLCGDCdtyy4UQ60RR7bkW5GvCA2VhckcIW0hhArzxUsDiOnSc20G+FkQQgQMnZAFBA0wkMGByDja4gMIIHEshMMFOhLtOC00w3LDDDqRcwMoFcMCBEkpIIAHHI3ycAhpm0UBBvg6jrDLLLsMs88w1m5CBp3aRIUTPKTvA8spBxyxzzV0U6loULsjQ9NMtvyz1CSL0W10US2TghQxSwGzCFlescITYwQQCACH5BAUAAHwALAUABQAyADIAAAf/gHyCg4SFg3hNKgwYMRNrEx4QBlUthpaXmINuDWhLMXETE29rHx8hIT0iIhMklZmvhkwkRx4enqCipKaoqnIPDxA0sLAlC0ZGtLYxjaG6qKkivw9YLjZ4w5c3aBgYyMmeoY6lzyK+0y4uNQ8N2IUaEBDcx8kMB1FMJYIlVk0KHtHn0tXIYqNCOz4qbMCL181GFTewWqgIQa3GQCdO4rQj8QSNQng2mhwUdEDOwCxO5rz4kADWAgZPnthQuCDfSEFuMKBU+WLFGoOYcjAY2hFNkZuGFjh50XNFBgyY6MCAMXSoMKSGiDhZ4TRDhgOWShgYMtUq1ks5umbY8IKNoQZD/+JOjXIWEwmvG/LGKNTCgN+4UOpmWpJ3w4ULdAc18OtXQUvBl9y8MHz4AyE1jA3cgJxJweELXbrYERRFgQI1alSc4ZxpTugxYyCQNG06MetLDGCPqfOCTwkStBXYvG2JyRgKyCnwoUGiOYnAxDG5SE7BQBEV2EnYjm7ISJjvYZYQwY7dCvdLC76o//JAw4L3C5Kct3QDh30cX+EvmH/pPo4xBwR4wH78GSLEgULgIGCABRpSxoNl4KDBgg0W0saFbYRRBRkckiFfhXxEccWIV1xQhAYoauAWiCQE4WIQNdyQogZ0gMhHDF544SIcTEDhIxRHgZgBEkjkCEMCDSSZ5P9jBdpB5JM1NqFkDqM1eAQKWKJQhyA05OBlDiI1iEMeeWC5Fx8J5FDFmkQwUaANIJghJwo1CnJDFUTk2QRQ3LXgBQiAmpEFIUnk2cGhOMxXwwmAAhrmIHcc2oEFFsDBHQQnZLoHCC5Y0gSllDpgA3EKnMACC5kiUWUhR4BqQQQFMMBaqXqciuoQmFxAaQS8FvAGZBiAsQUYYJz6QCYlmMErrw44EIZ5SDGxggxibDEsGBfA4sYJyzbrgAkT3HTECSnIQK21yg3TAhIReFvAu1SIMAwNa4CgRQrlmitGtu2UcIG7777LQR7VVDEIG0Qw8MAVXHBBBRX35tvpTRiMELBdwBxwoIQEHI8wghQmmDDFDDM0DDG+LMBQ1xgXZ6wxxxJ4DLLII5d8cm+QaRCGyxpv3PHHIYtMMhdabEAEcTc8oEfPMMs888gohFDgDR6sUAYLVISsxQk4OAHVTYEAACH5BAUAAHwALAUABQAyADIAAAf/gHyCg4SFg0k3DSoGDBAQNgxqB0VuhpaXmIdFCgwMT2g2jhgYRkdHHh42Gpmsly1QQzAwnZ+hEKOlp0tLMTEGVq2tCUQGBrGznqCipKcevDET0QZJwZd2JMXGsbRPtrjNvHHRb2sTTdWEZ00KCtnGsioddC2EbFEaDLzRE29wHx8G0PGpkIOdAjXZFBShxgoPFCP81vwLEQJCgmoNSJAwqIbEDYGCcsTw94FijyOtzlRRoUIjOyIXQQpKAqNkiB49RGBgFWUBy5Yk6Mg01OBmThFyYGCysqCpzwVMhloqEkIE0gcPqlhKQOaAU6hSLzXpIQfrgx70ChU5wLYpjbCY/zSYxYIFQqEkbPMWgZvJxgO6LlzcIVSETNcDq/hiStIjsIsaKAWV0EDZsB3FmQ48rsE5Kh8alClrxZypB+csWYYIagKltYa3pDEpQO3ESQ8+CRrobgAlZmxLLbI4mTPnhRUmuxuc+43pw4vnLxbQyUE9B2zmlhg8X7ECQpQq4HOkxW4ICnfub5oQWV+lBHlLdDLIz4BF/XoiFd4bwjM/g5MOAAKonyEVbGDgBhkEKOCAhBR44AYcWCChBcAwKAgeF2R4wQpcTGhBAxYKcoOGF2SBwoQR7BTiAV202EUPG0QgYwROhMgHBmPkOIYHMcwYwQk2ukABBXXUoUAUPjrwEf+DVgzp5FsyOCClAw9YiEYYYQz5giAPTOnADO4NuMEXX2CpYhQFSFlAAWsMOAQOcJZ5nRBrrmlCheTVIQScOIhAyAF1rknBez2UIcShOOxFCAWBFjABdjBc0UYZhoZgyB1SrMkBBxIw8NsCbQQhaRkUeFYIBgVsuqkUqmEGqhdBiHqFAphkoKoSSowQg2JPeIGEF7AG8QErQeCqhATIdhEWG1iggAISv3pRIytJoHAsshKMkMISvlVjwxVm5OEstBmE2VAZ2I6grhRgfCBUK0tcAcK84TrrBEPVrJDtulKYYMIMV8jBQAdptdDEECFQcAILJ+xBrxko3CaTDVSMIEVovyZMMcUMM3BBhRYppCCDGFtsAQYLDJ8wLwhBBCSVHRtc7O/GHXsMsshikHxyyg5jYWpYDVygMc0efxyyDCObrAfKe2SxXGw95MFx0TcjrbMeQkzw7nsY1EABCiyMfEIQFzyAxtboBAIAIfkEBQAAfAAsBQAFADIAMgAAB/+AfIKDhIWDJXZFVQcqCo4LZB13SYaVlpeHdw0LKiokJI5qBqNDQzAKTZSYq4ZJRQcHC7KeoAqipDAwDLsHLay/UWSwsbOeoaMGproMT2g2ByW/lmwNGhrCsMW1t8nLT082NhBoN9KFN1BQ1tixOVFWbgmCCXg0TSq6zeIQEBgH5oKiNGigbl0DOtFWuRrijB8GDEbUmCuSYyBBDQ3snAHIpwiDfg+NHBlSgVWUKjkqDozCkVCJBSGPHPFgYBUNIkRQprTS0lARDDI9CF1wqUWHDjirEPHV09ANI0KXLIlRpNINDkeRdmDa1KkHqTFieHBj6IsFC1mZdL3UZGqMOBP/ahI6EOHs2TtrMamBO6GvHUJX7FqQ8DevpRIe+k54A2NQFAcR6lqAYxgTkcVr4KxhiiVy5BnyKl/yAAfOhw9E+cjwHOGBaEwHTp/2wOeGg9sRHJR7felDiN8h2Cy5ffsEb0xGfvfo0SADcQdZjl8isXy5gSAFsjuAIN1SExHgRWBIkT17ju6V7IQXMWFE+QI80RdK8qD+gxDvCySUT8j+Azn57cefIFgUiMUDUnCgIAcDEpKECxC6IIIYC3JQRYOC0BChCyG0wYESINqAIR851GBiDUusACKILowIQxYw1vDEERLUKAESI37ghBMwHnCDjRKMgCEbcxS5o1on2DhC/wgNGvDCky+IIEgPI1Q5whahoVeBCyus8GRjgkhhpRQx8EdCBhl0+UJ8fFAwghRwyqBWd3jMgSaacRCigQlSmODnBujFsMEGd+42yAZ+mjDFFLQdp8AFgw66hCE0pKDoFDNQMQRvGlzgKaQrsFGJDYvOYKoMElXWaRddfPqPJU6YOgMXXKRwhGEGjFHHGKxe0KglCXwxKxdUaKHFCnO21MIHYVBAwa5dSMmKEMQam0IKexjR0hBdfNGss3W49osbFBR7bQoyyIBEDDSwYgUEFwiBAw7eOiuCgKskUMO56YqxxRYsUPCBAVUNUoQCMaxwRRtlCCHvF96W2RIDLPTrL2oYerDAwgl7gACCGSiggIQXQQRxRcPy4jCGCl1Z4YQY/m6B8cYnePxxyCOXvHDDOHzAVVcdrADG0BqfULPHIIvsBck7i1CwaHRMIETRR5uRNBI5X2BEu+jR8IQIGeAQRMhXUDDHB0NwDVAgACH5BAUAAHwALAUABQAyADIAAAf/gHyCg4SFgwlMN0VVGgcHZFAdUTQlhpaXmIc0TQ1QGhpkjgcLpCokJAdRlZmshiU3OTkNnZ+hjqWmJAoKajmtvxV0RERVsrSgorinu2oGBlW/mEZNHR3EsbOejbcLKqa7Cs7iNNGEJRcWFtXDxQ1NdGxJFWdnfG5WUVDM4kNDMNDlWqBIl25dEzsJWpWIooIfDBgMFlT45eZEBILqmjApJ+jMDQX+IDJgQEIhiggoCQqZyPEQFJEMnjxRwWoDypsO4LS0FGWkTDQ2NEhz4OAmhyc7L9GICRQChBuW7owgSrSAjaSYaDR1aiOJIQoFqDp4gzVTEacQMGBYUIhMgbdE/7+UZbUgrVojGwcJeftWipW5rNBgMEJYASEOfAtMAMyqCOEjkAfJSayFcSsbRzxoFspHDwfEBURYZpVDs+arfD6rHs3KjYclsJe08KBEtRnWrBjE2B2jyQolwJW4wJ3pQIw4cSaoKCNBAnDUxC0VmUB9AgMwzZsDjG6ISfUJRmZkl5CXu6E3b9asiTGiffOE5g3Bmf9hTfv78OMT+sD/Axwq941Qnn6ChGBgCBOwIMWCUnRA4CBWHBhCDDiYYKEJSD3IRxM9dNgDBFlcaMIDGvKhgAgo9mAABiZM4eIVJXqAIooN3OHiFDPMQMeDLTzgoxxysMEHCjnm+MGDJPjoI/9ZfISQIxdc7PFgCFhg4aNhfNxABZRUUOGBfge4IKaV5V3QJRVasBCfGw/UIKYLRhDSgBZ0apHCCuZhkEUNfLpwRyEr2JlCCjJgEN0CTjiRxZ6GGsLCoDLIAIYBuOXgxAtzKIpFC5YwEGmkYpxQkmVVzLHCC5g6AQUmLsgghhhbbMECBIyRsEIGK5z6Aq2ZdAHrFmDowUIWQmIVwwYZJJvrGyxlQgGwwrJwAhK8cqTGChtkm2wGIeRXExgshHvCCSAIcYQdrTDBwApdXHBBtsiu0dID4p6wBwggmIHCBnFgWUEFeESxgBE1UEDBGO26m60RzZZjAAr24psvCigg4UVaEEFcUYYQQuDwRRgG14GwuyscUBYTLkhsRh4UW4yxxhx7DLLBI8dgWRM1mLFyxS5nvHHHX3xMMxxR4EbDEnXwfLHPMcv8gg1/xTfEB05c8MXGFGyARb9RcxQIADs=" class="skidka-widget__loading-img">',
        '</div>'
      ].join('\n'),
      popup: {
        'success': [
          '<div class="modal">',
            '<button type="button" class="modal__close js-close-modal">Закрыть</button>',
            '<h2 class="modal__title">Благодарим вас за заказ!</h2>',
            '<p class="modal__text">Информация отправлена на ваш телефон по SMS. Желаем приятных эмоций!</p>',
            '<button type="button" class="modal__btn js-close-modal">Выбрать ещё купоны</button>',
          '</div>'
        ].join('\n'),
        'error': [
          '<div class="modal">',
            '<button type="button" class="modal__close js-close-modal">Закрыть</button>',
            '<h2 class="modal__title">Сервис недоступен</h2>',
            '<p class="modal__text" style="margin-bottom:0">Попробуйте повторить запрос позднее</p>',
          '</div>'
        ].join('\n')
      }
    },

    showLoading: function () {
      // this.container.innerHTML = this.templates['loading'].replace(/#widgetImagesUrl#/gi, this.widgetImagesUrl);
      this.container.innerHTML = this.templates['loading'];
    },

    generateRandom: function () {
      var _self = this;

      var random = Math.floor(Math.random() * _self.ln);

      if (random === _self.prev) {
        random = _self.generateRandom();
      }

      this.prev = random;

      return random;
    },

    setListeners: function () {
      var _self = this;

      _self.refresh.addEventListener('click', function (e) {
        e.preventDefault();

        _self.showLoading();
        setTimeout(_self.parse.bind(_self), 200);
      }, false);
    },

    watchOrderBtn: function (id) {
      var _self = this;

      var config = {
        dealId: id,
        msisdn: _msisdn
      };

      var orderBtn = document.getElementById('js-order');
      var sendOrder = function (e) {
        e.preventDefault();

        _self.sendRequest(_base, '?r=default/sendOrder', config)
          .then(function (response) {
            if (response.code === 0) {
              _self.showPopup('success');
            } else {
              _self.showPopup('error');
            }
          })
          .fail(function (err) {
            _self.showPopup('error');
          });
      };

      orderBtn.removeEventListener('click', sendOrder);
      orderBtn.addEventListener('click', sendOrder, false);
    },

    parse: function (deals) {
      var rn, frag, deal;

      if (!this.ln) this.ln = this.deals.length;

      rn = this.generateRandom();
      deal = this.deals[rn];

      frag = this.templates['main']
        .replace(/#discount_percent#/ig, deal.discount_percent)
        .replace(/#img#/ig, 'http://skidka.mts.ru/' + 'img/700X340/' + deal.image)
        .replace(/#deal_id#/ig, deal.deal_id)
        .replace(/#title#/ig, deal.title)
        .replace(/#price#/ig, deal.price > 0 ? 'Стоимость купона ' + deal.price + ' руб.' : 'Бесплатно');

      this.container.innerHTML = frag;
      this.watchOrderBtn(deal.deal_id);
    },

    sendRequest: function (base, uri, config) {
      var config = config || {};

      return $.ajax({
        url: base + uri,
        type: 'POST',
        data: config
      });
    },

    showPopup(type) {
      function closePopup(e) {
        e.preventDefault();

        if (e.target.classList.contains('js-close-modal')) {
          modal.removeEventListener('click', closePopup);
          document.body.removeChild(modal);
        }
      }

      var modal = document.createElement('div');
      modal.className = 'popup';
      modal.innerHTML = this.templates.popup[type];

      modal.addEventListener('click', closePopup, false);

      document.body.appendChild(modal);
    },

    init: function () {
      var _self = this;

      _self.showLoading();

      _self.sendRequest(_base, '?r=default/defaultCity')
      //Current City
        .then(function (response) {
          var config = {
            'filter': {
              cities: response.payload.data.defaultCity.city_id
            }
          };

          _self.sendRequest(_base, '?r=default/filterDeals', config)
          //Deals for Current City
            .then(function (response) {
              _self.deals = response.payload.deals;

              _self.setListeners();
              _self.parse();
            });
        })
        .fail(function (err) {
          console.warn('--> FAIL:', err.statusText);
        });

    }
  };

  com.rooxteam.widgets.Skidka = Skidka;
})();